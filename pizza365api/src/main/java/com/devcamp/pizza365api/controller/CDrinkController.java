package com.devcamp.pizza365api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365api.models.CDrink;
import com.devcamp.pizza365api.service.CDrinkService;

@RestController
public class CDrinkController {
    @Autowired
    public CDrinkService cDrinkService;

    @CrossOrigin
    @RequestMapping("/drinks")
    public ArrayList<CDrink> getCDrinkList() {
        return cDrinkService.getDrinkList();
    }
}
