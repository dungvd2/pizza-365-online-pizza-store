package com.devcamp.pizza365api.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import java.util.Date;
import java.util.Locale;

@CrossOrigin
@RestController
public class CPizzaCampaign {
	@CrossOrigin
	@GetMapping("/campaigns")
	public String getDateViet() {
		DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
		LocalDate today = LocalDate.now(ZoneId.systemDefault());
		String dayOfWeek = String.format("%s", dtfVietnam.format(today));
		System.out.print(dayOfWeek);
		String campaign = "";
		if(dayOfWeek.equals("Thứ Hai")) {
			campaign = "Hôm nay là " + dayOfWeek + " giảm giá 10%";
		}
		if(dayOfWeek.equals("Thứ Ba")) {
			campaign = "Hôm nay là " + dayOfWeek + " giảm giá 25%";
		}
		if(dayOfWeek.equals("Thứ Tư")) {
			campaign = "Hôm nay là " + dayOfWeek + " giảm giá 50%";
		}
		if(dayOfWeek.equals("Thứ Năm")) {
			campaign = "Hôm nay là " + dayOfWeek + " giảm giá 25%";
		}
		if(dayOfWeek.equals("Thứ Sáu")) {
			campaign = "Hôm nay là " + dayOfWeek + " giảm giá 50%";
		}
		if(dayOfWeek.equals("Thứ Bảy")) {
			campaign = "Hôm nay là " + dayOfWeek + " mua 1 tặng 1";
		}
		if(dayOfWeek.equals("Chủ Nhật")) {
			campaign = "Hôm nay là " + dayOfWeek + " mua 1 tặng 1";
		}


		return campaign;
	}

	@CrossOrigin
	@GetMapping("/devcamp-infor")
	public String getInfor(@RequestParam(value = "name", defaultValue = "Pizza Lover") String fullName) {
		DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
		LocalDate today = LocalDate.now(ZoneId.systemDefault());
		return String.format("Hello %s ! Hôm nay %s, mua 1 tặng 1.", fullName, dtfVietnam.format(today));
	}

	@CrossOrigin
	@GetMapping("/devcamp-simple")
	public String simple() {
		return "test campaign";
	}

	@CrossOrigin
	@GetMapping("/devcamp-welcome1")
	public String nice() {
		DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
		Date now = new Date();
		return String.format("Hello devcamper, now it is %s.", dateFormat.format(now));
	}

	

}

/*
//Bài giải cho bài 63.30 TDD Làm rest api dice
//Tách class này ra file riêng
class RandomMain1 {
	public static double randomNumber() {
	     double x = Math.random();
	     return x;
	}
	public static double randomNumber(int max, int min) {
		
	     double x = (Math.random()) * ((max - min) + 1) + min;
	     return x;
	}
	public static int randomNumberInt(int max, int min) {
	     int  x = (int)((Math.random()) * ((max - min) + 1) + min);
	     return x;
	}
}
*/