package com.devcamp.pizza365api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365api.models.CMenu;
import com.devcamp.pizza365api.service.CMenuService;

@RestController
public class CMenuController {
    @Autowired
    public CMenuService cMenuService;

    @CrossOrigin
    @RequestMapping("/combomenu")
    public ArrayList<CMenu> getMenuList() {
        return cMenuService.getAllMenu();
    }
}
