package com.devcamp.pizza365api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.pizza365api.models.CDrink;

@Service
public class CDrinkService { 
    
    public ArrayList<CDrink> getDrinkList() {
        CDrink drink1 = new CDrink("TRATAC", "Trà Tắc");
        CDrink drink2 = new CDrink("COCA", "Cocacola");
        CDrink drink3 = new CDrink("PEPSI", "Pepsi");
        CDrink drink4 = new CDrink("LAVIE", "Lavie");
        CDrink drink5 = new CDrink("TRASUA", "Trà sữa trân châu");
        CDrink drink6 = new CDrink("FANTA", "Fanta");
        CDrink drink7 = new CDrink("AQUAFINA", "Aquafina");
        CDrink drink8 = new CDrink("STING", "Sting");

        
        ArrayList<CDrink> allDrink = new ArrayList<>();
        allDrink.add(drink6);
        allDrink.add(drink5);
        allDrink.add(drink4);
        allDrink.add(drink3);
        allDrink.add(drink2);
        allDrink.add(drink1);
        allDrink.add(drink7);
        allDrink.add(drink8);


        return allDrink;
    }
}
