package com.devcamp.pizza365api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.pizza365api.models.CMenu;

@Service
public class CMenuService {
    public ArrayList<CMenu> getAllMenu() {
        CMenu comboS = new CMenu("S", 20, 2, 200, 2, 150000);
        CMenu comboM = new CMenu("M", 25, 4, 300, 3, 200000);
        CMenu comboL = new CMenu("L", 30, 8, 500, 4, 250000);

        ArrayList<CMenu> allMenu = new ArrayList<>();
        allMenu.add(comboS);
        allMenu.add(comboM);
        allMenu.add(comboL);

        return allMenu;
    }
}
