package com.devcamp.pizza365api.models;

public class CDrink {
    private String maNuocUong;
    private String tenNuocUong;
    
    public CDrink(String maNuocUong, String tenNuocUong) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
    }

    public String getMaNuocUong() {
        return maNuocUong;
    }

    public String getTenNuocUong() {
        return tenNuocUong;
    }    

    
}
