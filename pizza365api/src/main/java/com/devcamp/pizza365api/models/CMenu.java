package com.devcamp.pizza365api.models;

public class CMenu {
    private String size;
    private int duongKinh;
    private int suon;
    private int salad;
    private int drink;
    private int price;

    public CMenu(String size, int duongKinh, int suon, int salad, int drink, int price) {
        this.size = size;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.drink = drink;
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public int getDuongKinh() {
        return duongKinh;
    }

    public int getSuon() {
        return suon;
    }

    public int getSalad() {
        return salad;
    }

    public int getDrink() {
        return drink;
    }

    public int getPrice() {
        return price;
    }

    
}
